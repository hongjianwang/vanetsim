/////////////////////////////////////////////////
	VANET simulator and sth else
	Hongjian Wang
	sishenhadesi@163.com
/////////////////////////////////////////////////

This is the code base of my INFOCOM'13 paper 
Compressive sensing based monitoring with vehicular networks
http://ieeexplore.ieee.org/xpls/icp.jsp?arnumber=6567092&tag=1

Project Info:
	The VANET simulator on the real data trace of Shanghai Grid.
	
	Implemented in Python (rapid development, low run-time efficiency, don't
laugh at me. Python is cool after all)

	Besides the simulator there are many other relavent MATLAB code for data
analysis, and data collected from the simulation.


Newly update at 3/20/2013
I call this update "revolution"

Project Tree:

/-- BPsolver/
		|-	find_lambdamax_l1_ls.m	-- BP solver parameter searcher
		|-	find_lambdamax_l1_ls_nonneg.m	-- BP solver parameter searcher
		|-	l1_ls.m					-- BP solver
		|-	l1_ls_nonneg.m			-- BP solver
		|-	l1_ls_usrguide.pdf		-- BP solver doc
 |-	CSrecover/
		|-	fourier_matrix.m	-- basis generator
		|-	haar_matrix.m		-- basis generator
		|-	isprocess.m			-- process monitor, used in the MainSim_matlab.m
		|-	iterated_haar_recover.m 	-- one recover method
		|-	recover_analysis.m	-- most recover method
		|-	sort_basis.m		-- basis generator
		|-	speed.m			-- speed plot, to do the intuitive correlation analysis
 |-	entropyAnalysis/
		|-	Conditional_Entropy.m	-- initial version of function CE (abandon)
		|-	Entropy.m			-- Calculate Entropy (abandon)
		|-	entropy_analysis.m	-- self-explaned (Important!)
		|-	Joint_Entropy.m		-- matlab function (Important!)
		|-	Marginal_Entropy.m	-- matlab function (Important!)
 |-	ICTsimulator/					-- Python inter-contact time simulator
		|- 	car.py
		|-	createSurfaceFit.error.m
		|-	cs_recover.m
		|-	ga_para_search.m
		|-	main.py
		|-	simulator.py
 |-	res/							-- some final experiment results
 |-	RTsimulator/
		|- 	TraceTracker/			-- Python real trace Simulator
			|-	__init__.py
			|-	analyzer.py
			|-	AP.py
			|-	car.py
			|-	recored.py
			|-	simulator.py
			|-	traceTool.py
		|-	mainSim.py
		|-	Mainsim_matlab.m
 |-	tmp/							-- some intermediate experiment results
 |-	README.txt



========================= Obsolete content =========================
Project Tree:
	
/--	consummate_data  	-- correct simulation result backup
	old_data			-- old simulation result with pitfalls
	Simulator			-- Python implemented Simulator packege
		|-	analyzer.py
		|-	AP.py
		|-	car.py
		|-	recored.py
		|-	simulator.py
		|-	traceTool.py
	A_rec*.txt			-- simulation result 	y = A * v
	v_rec*.txt
	y_rec*.txt
	broken_data			-- id of cars with broken data in real trace
	Conditional_Entropy.m	-- initial version of function CE (abandon)
	entropy.fig			-- entropy result figure
	Entropy.m			-- Calculate Entropy (abandon)
	entropy_analysis.m	-- self-explaned (Important!)
	find_lambdamax_l1_ls.m	-- BP solver parameter searcher
	find_lambdamax_l1_ls_nonneg.m	-- BP solver parameter searcher
	Fourier_Basis_1000 	-- 1000 x 1000 basis matrix
	fourier_matrix.m	-- basis generator
	Haar_Basis_1000		-- 1000 x 1000 basis matrix
	haar_matrix.m		-- basis generator
	isprocess.m			-- process monitor, used in the MainSim_matlab.m
	iterated_haar_recover.m 	-- one recover method
	Joint_Entropy.m		-- matlab function (Important!)
	l1_ls.m				-- BP solver
	l1_ls_nonneg.m		-- BP solver
	l1_ls_usrguide.pdf	-- BP solver doc
	mainSim.py			-- simulator python caller (simulation)
	MainSim_matlab.m	-- simulator matlab caller (simulation + analysis)
	Marginal_Entropy.m	-- matlab function (Important!)
	READEME.txt			-- this doc
	recover_analysis.fig	-- recover result figure
	recover_analysis.m	-- most recover method
	sort_basis.m		-- basis generator
	spatio_ce_unique.fig	-- some result figure
	speed.m				-- speed plot, to do the intuitive correlation analysis