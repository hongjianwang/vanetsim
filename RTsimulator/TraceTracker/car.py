# class car describe one car
# each car has its own trajectory, which consists of record
import math
import geohash
import random
import copy
from record import record

class car:
    cid = 0
    path = list()
    curTime = 0
    lastl = 0
    lastr = 1
    msg_queue = []
    v = 0
    rand_code = 0
    
    def __init__(self, cid = 0, id = 0):
        self.id = id
        self.cid = cid
        self.path = list()
        self.curTime = 0
        self.lastl = 0
        self.lastr = 1
        self.msg_queue = list()
        self.v = 0  # we evaluate a real velocity according to the record
        self.rand_code = random.random() * 2 -1 # assign rand [-1,1].
        # following are used for delegation forwarding
        self.quality = 0
        

    def __str__(self):
        return ('#car: ' + str(self.cid) +', coordiante: ' +
            str(self.getCurCoordinate()) + ' ,curTime: ' +
            str(self.curTime))
        
    def addRec( self, rec ):
        self.path.append(rec)
        if rec.time <= 8 * 3600:
            self.v = rec.v

    def getCurCoordinate( self ):
        rec = self.getRecByTime( self.curTime )
        return [ rec.x, rec.y]

    def getCurGeoHash( self, resolution = 8):
        x, y = self.getCurCoordinate()
        return geohash.encode(y,x,resolution)
        
    def getCurGeoHashRegion( self ):
        gh = self.getCurGeoHash()
        return geohash.expand( gh )
    
    def getRecByTime( self, t ):
        if t < self.path[0].time:
            return self.path[0]
        elif t > self.path[len(self.path)-1].time:
            return self.path[len(self.path)-1]
        else:
            return self.getRecByTimeAssit( t, 0, self.path.__len__()-1 )

    def getRecByTimeAssit( self, t, l, r ):
        if l == r-1:
            self.lastl = l
            self.lastr = r
            ra = self.path.__getitem__(l)
            rb = self.path.__getitem__(r)
            ta = ra.time
            tb = rb.time
            if ta == t:
                return ra
            elif tb == t:
                return rb
            elif ta < t and tb > t:
                rc = ra
                rc.time = t
                rc.x = ra.x + (rb.x-ra.x)*float(t-ta)/float(tb-ta)
                rc.y = ra.y + (rb.y-ra.y)*float(t-ta)/float(tb-ta)
                return rc
            else:
                return 0
        mid = (l+r)/2
        if t <= self.path.__getitem__(mid).time:
            return self.getRecByTimeAssit( t, l, mid )
        else:
            return self.getRecByTimeAssit( t, mid, r )
            
    def distanceTo( self, c ):
        c_rec = c.getRecByTime(c.curTime)
        cx = c_rec.x
        cy = c_rec.y
        x,y = self.getCurCoordinate()
        d = math.sqrt( (x-cx)**2 + (y-cy)**2 )
        return d*111199.233

    def msg_initialize( self ):
        ''' Initialize a message in current vehicle.
            The message have following format:
                [ y, v_min, v_max, id_1, id_2, ... ]
            where
                y = a_1 * v_1 + a_2 * v_2 + ... + a_n * v_n
        '''
        val = self.v * self.rand_code
        self.msg_queue.append([ val, self.v, self.v, self.cid ])
        return self.msg_queue
    
    def msg_receive( self, msg ):
        ''' collect one piece of information for current car
            Only if the current vehicle has a higher speed than v_max,
            or lower speed than v_min, the speed of current vehicle is
            mixed into current message.
        '''
        rec_flag = False
        if self.v <= msg[1] and self.v * 3 > msg[1] :
            msg[0] += self.v * self.rand_code
            msg[1] = self.v
            msg.insert( 3, self.cid )
            self.msg_queue.append( msg ) 
            rec_flag = True
        elif self.v >= msg[2] and msg[2] * 2 > self.v :
            msg[0] += self.v * self.rand_code
            msg[2] = self.v
            msg.append( self.cid )
            self.msg_queue.append( msg ) 
            rec_flag = True
        return rec_flag    
  
    def msg_forward( self, neighbors ):
        ''' forward a message to a randomly chosen neighbors
            Note: this is an trial, the forwarding happends condionally
        '''
        cnt = 0
        for msg in self.msg_queue:
            dsts = copy.copy(neighbors)
            ml = msg[1:]
            re = random.choice(dsts)  
            while re.cid in ml:
                dsts.remove( re )
                if dsts != []:
                    re = random.choice(dsts)
                else:
                    re = []
                    break
            if re != []:
                if re.msg_receive( msg ):
                    self.msg_queue.remove(msg)
                    cnt += 1
        return cnt, re

    def msg_send( self, dst ):
        cnt = 0
        for msg in self.msg_queue:
            dst.msg_queue.append( msg )
            self.msg_queue.remove( msg )
            cnt += 1
        return cnt

    def getV_interval( self, tc, delta = 300 ):
        tmp = 100000000;
        v = -1;
        for r in self.path:
            if r.time < tc and tc - r.time < delta and r.time - tc < tmp:
                tmp = r.time - tc
                v = r.v
            elif r.time - tc > delta:
                return v
       
        return v
            
            