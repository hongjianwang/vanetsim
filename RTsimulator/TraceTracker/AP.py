import geohash
from car import car

class AP:
    aid = 0
    x = 0
    y = 0
    curTime = 0
    msg_queue = []
    
    def __init__(self,line, id = 0, t = 0):
        self.aid = id
        line = line.split()
        if len(line) == 2:
            self.x = float(line[0])
            self.y = float(line[1])
        self.msg_queue = list()
        self.curTime = t

    def distanceTo(c, t ):
        a = c.getRecByTime(t)
        if a == 0:
            return 1e9
    
    def getCurGeoHash( self, resolution = 8):
        return geohash.encode(self.y, self.x, resolution)
        
    def getCurGeoHashRegion( self ):
        gh = self.getCurGeoHash()
        return geohash.expand( gh )
