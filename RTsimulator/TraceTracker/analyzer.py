from car import car
import fileinput
import copy
import random
import time


def l1_ls_parameterGen( cars_list, msg_queues ):
    ''' This function will generate 3 files which can be used by
        the l1_ls algorithm to estimate the original speed matrix.
        The 2 parameters used in the matlab l1_ls algorithm are:
            y   --- in y_rec.txt
            A   --- in A_rec.txt
        Besides, we need an extra matrix to verify:
            v   --- in v_rec.txt
    '''
    # collect all the non-empty messages
    msgs = list()
    for c in cars_list:
        for msg in c.msg_queue:
            if msg != []:
                msgs.append( msg )
    
    for pt in range(10, 101, 10):
        msgsF = msgChoose( pt, msgs )
        writeParameterIntoFile( cars_list, msgsF, pt )
  
  
def msgChoose( percentage, msgs ):
    msgsbk = copy.copy( msgs )
    cnt = 0
    sum = len(msgs) * percentage / 100
    msgres = list()
    while cnt < sum:
        m = random.choice( msgsbk )
        msgres.append( m )
        msgsbk.remove( m )
        cnt += 1
        
    return msgres
    
    
def writeParameterIntoFile( cars_list, msgs, percent = 100 ):
    # construct the v and write out
    v = list()
    for c in cars_list:
        v.append( c.v )
    
    vrec_name = ''.join( ['../tmp/RT_v_rec', str(percent), '.txt'] )
    fout = open( vrec_name, 'w' )
    sv = str(v)
    sv = sv.replace('[','')
    sv = sv.replace(']','')
    fout.write( sv )
    fout.close()
    
    # construct y and A, then write out respectively
    A = list()
    y = list()
    for msg in msgs:
        y.append( msg[0] )
        a = list()
        for c in cars_list:
            if c.cid in msg:
                a.append( c.rand_code )
            else:
                a.append( 0 )
        A.append( a )
    
    del msg
    del a
    del c
    
    # write out y
    yrec_name = ''.join( ['../tmp/RT_y_rec', str(percent), '.txt'] )
    fout = open( yrec_name, 'w' )
    sy = str(y)
    sy = sy.replace('[','')
    sy = sy.replace(']','')
    fout.write(sy)
    fout.close()
    
    # write out A
    Arec_name = ''.join( ['../tmp/RT_A_rec', str(percent), '.txt'] )
    fout = open( Arec_name, 'w' )
    sA = str(A)
    sA = sA.replace('[','')
    sA = sA.replace('],','\n')
    sA = sA.replace(']','')    
    fout.write( sA )
    fout.close()
    
    
def entropy_parameterGen( cars_list, thStart = 6, thEnd = 10 ):
    print 'Start generate velocity to do entropy analysis.'
    t1 = time.clock()
    v = list()
    tmpv = 0
    t = thStart * 3600
    te = thEnd * 3600
    while t <= te:
        tmpt = []
        for c in cars_list:
            tmpv = c.getV_interval( t )
            if tmpv !=  -1:
                tmpt.append( tmpv )
            else:
                tmpt.append( 0 )
        v.append( tmpt )
        t += 60
    
    # now we have get the velocity matrix in v
    # each row refers to the velocity of all cars at current timestamp
    
    fout = open( '../tmp/RT_velocities.txt', 'w' )
    for row in v:
        srow = str(row)
        srow = srow.replace( '[', '' )
        srow = srow.replace( ']', '' )
        fout.write( srow )
        fout.write( '\n' )
    fout.close()
    
    t2 = time.clock()
    print 'Entropy analysis parameter collection finished in %f seconds' \
            % (t2 - t1)
    return v