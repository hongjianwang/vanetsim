# the record contains one piece of information for one car at
# one moment.
# import geohash

class record:
    cid = 0
    date = ''
    time = 0
    x = 0
    y = 0
    v = 0
    direction = 0
    status = 0
    #geo_loc = ''
    
    def __init__( self, line=''):
        p = line.replace(',',' ')
        p = p.split();
        if p.__len__() != 8:
            self.cid = 0
            self.date = ''
            self.time = 0
            self.x = 0
            self.y = 0
            self.v = 0
            self.direction = 0
            self.status = 0
            #self.geo_loc = ''
            return
        else:
            self.cid = int(p[0])
            self.date = p[1]
            t = p[2].split(':')
            self.time = int(t[0])*3600 + int(t[1])*60 + int(t[2])
            self.x = float(p[3])
            self.y = float(p[4])
            self.v = int(p[5])
            self.direction = int(p[6])
            self.status = int(p[7])
            #self.geo_loc = geohash.encode(self.y,self.x,8)

    def __str__( self ):
        return ('#cid: '+str(self.cid)+', '+str(self.time)+', '+
                str(self.x)+', '+str(self.y))