# simulate the whole scenario
import os
import os.path
import fileinput

import time
import geohash
import copy
import random
import dircache
from record import record
from car import car
from AP import AP
from traceTool import traceTool

class simulator:
    ''' The highest class in the hierarchy structure of the simulator.
    There are currently 6 member variables here, they are:
        time            --- capture the current system timestamp
        cars            --- a list of all the cars within system
        APs             --- a list of all the APs within system
        filelist        --- the original input, a set of files
        filenum         --- number of original input files
        file_directory  --- The parent dir of filelist
    Besides, there are currently 13 member functions, they are:
        __init__()
        setCurTime( t )
        getNeighbors( src_id, threshold )
        getNeighborsGeo( src_id, tag = 0 )
        getRandNeighborGeo( src )
        
        getBrokenFile()
        deleteBrokenFile()
        generateFilteredTrace()
        
        searchGeometricCenter()
        findAPNeighbor( ap_id = 0 )
        carsNeighborAnalysis( nn = 1 )
        
        packetRandomInitialize()
        packetRandomWalkForwarding()
    '''
    time = 0
    cars = dict()
    APs = list()
    
    def __init__(self, fdir, rangel = 0, ranger = 100):
        self.time = 0
        # file initialize 
        self.trace = traceTool( fdir )
        filelist = self.trace.filelist
        # initalize all the cars
        
        print 'Start scanning the file ...'
        t1 = time.clock()
        random.seed(1)
        cnt = 0
        for f in filelist[rangel:ranger]:
            fin = open( fdir +'/'+ f, 'r')
            c = car( int(f[5:]), cnt )
            cnt += 1
            
            for line in fin:
                tmpR = record(line)
                c.addRec( tmpR )

            self.cars[c.cid] = c
            fin.close()

        t2 = time.clock()
        print 'File scanning finished, time elapsed %f' % (t2-t1)

        # initalize all the APs
        #ap0 = AP(121.7255973815918, 31.212244033813477, 0, self.time)
        cnt = 0
        fin = open('../res/ap300.txt');
        for l in fin:
            self.APs.append( AP(l, cnt, 0) )
            cnt += 1;
        fin.close()
        
        
    def setCurTime( self, t ):
        self.time = t
        for ap in self.APs:
            ap.curTime = t
        for c in self.cars.values():
            c.curTime = t
            
    def getNeighbors( self, src_id, threshold, tag = 0):
        ''' Get the neighbors of the source car or AP 
            using Euclid distance.
            ## if tag == 0, get the neighbors of a car ##
            ## if tag == 1, get the neighbors of an AP ##
        '''
        neighbors = list()
        if tag == 0:
            src = self.cars[src_id]
        elif tag == 1:
            src = self.APs[src_id]
            
        for n in self.cars.values():
            if src.distanceTo(n) <= threshold and src_id != n.cid:
                neighbors.append(n)

        return neighbors
   
    def getNeighborsGeo( self , src_id, tag = 0 ):
        ''' Get the neighbors of the source car or AP 
            using geohash method.
            ## if tag == 0, get the car neighbors ##
            ## if tag == 1, get the AP neighbors  ##
        '''
        neighbors = list()
        if tag == 0:
            src = self.cars[src_id]
        elif tag == 1:
            src = self.APs[src_id]
        
        src_ghn = src.getCurGeoHashRegion()
        for n in self.cars.values():
            n_gh = n.getCurGeoHash()
            if (n_gh in src_ghn) and (src_id != n.cid ):
                neighbors.append(n)
        return neighbors
    
    def getRandNeighborGeo( self, src ):
        neighbors = self.getNeighborsGeo( src.cid, 0 )
        re = random.choice(neighbors)
        return re
        
    
    
    # following two functions are used once at the very beginning
    # to remove the pitfalls in the dataset
    def getBrokenFile( self, fileName = '../tmp/RT_broken_data'):
        ''' Get the file list of all the files containing
            broken data.
            broken data is defined as:
                trace starts after 1:00 am
                trace ends before 10:00 pm
                cars have the same location for all day long
        '''
        cars = self.cars.values()
        broken_data = self.trace.brokenTraceList( cars, fileName )
        return broken_data

    def deleteBrokenFile( self, fileName = '../tmp/RT_broken_data' ):
        ''' delete the file containing broken data,
            the broken data file list is given in an 
            additional file named
                'broken_data'
        '''
        self.trace.brokenTraceDelete( fileName )
        
    
    # the following function are used to calculate the 
    # geometric center of the cars' movement
    # so that we can distribute APs at these centers
    def searchGeometricCenter( self, nAPs = 100):
        ''' Find a geohash region which has the highest
            hit ratio '''
        print "Start searching for Geometric center..."
        t1 = time.clock()
        t = 0
        key_locus = dict()
        while t <= 72000:
            self.setCurTime(t)
            for i in self.cars.values():
                h = i.getCurGeoHash()
                if key_locus.has_key(h):
                    key_locus[h] += 1
                else:
                    key_locus[h] = 1
            t += 600
    
        locus_sor = sorted( key_locus.keys(), key = lambda x: key_locus[x],
                        reverse = True)
        
        fin = open('../tmp/ap' + str(nAPs) + '.txt', 'w')
        for a in locus_sor[:nAPs]:
            coor = geohash.decode(a)
            line = ' '.join( [str(coor[1]), str(coor[0])] )
            fin.write(line + '\n')
            print coor, key_locus[a]
        fin.close()
        
        t2 = time.clock()
        print "Finished searching :-) using time %f" % (t2-t1)
        return locus_sor
        
    # delegation forwarding parameter setting
    def delegation_forwarding_parameter( self ):
        print "Start setting delegation forwarding parameters ..."
        t1 = time.clock()
        t = 0
        while t <= 72000:
            self.setCurTime(t)
            for ap in self.APs:
                nc = self.getNeighborsGeo(ap.aid, 1)
                for c in nc:
                    c.quality += 1
            t += 600

        fo = open('../res/delegation_forwarding.txt', 'w')
        for k in sorted(self.cars.keys()):
            c = self.cars[k]
            fo.write( ' '.join( [str(c.id), str(c.cid), str(c.quality), '\n'] ) )
        fo.close()
        
    
    # the following functions are used to analyze the
    # connectivity of the VANET
    # we inituitively list the neighbors of each car
    # and each AP
    def findAPNeighbor( self, ap_id = 0 ):
        ''' Find the neighbors of the ap with ap_id
            at each given time '''
        print "Starting searching APs' neighbors ..."
        t1 = time.clock()
        t = 0
        while t <= 72000:
            neighID = []
            self.setCurTime( t )
            neigh = self.getNeighborsGeo( ap_id, 1 )
            for n in neigh:
                neighID.append(n.cid)
            num_neigh = len(neigh)
            if num_neigh >= 1:
                print "At time", t, num_neigh, "neighbors around. They are:", neighID
            t += 600
        t2 = time.clock()
        print "Finished searching APs' neighbors. Using time %f" % (t2-t1)
        
    def carsNeighborAnalysis( self, nn = 1):
        ''' neighborhood analysis for cars
            we will list the number of cars with more than r neighbors
            at each timestamp
        '''
        print "Start cars neighborhood analysis ..."
        print "We will analyze cars with more than", nn, "neighbors."
        t1 = time.clock()
        t = 0
        while t <= 72000:
            cnt = 0
            active_car_list = []
            self.setCurTime(t)
            # check the number of neighbors each car has
            for i in self.cars.keys():
                num_neigh = len(self.getNeighborsGeo( i ))
                if num_neigh >= nn: 
                    cnt += 1
                    active_car_list.append( i )
            if cnt >= 1:
                print "At time", t, "we have", cnt, "cars. They are:", active_car_list
            t += 600
        t2 = time.clock()
        print "Finish cars neighbor analysis. Using time %f" % (t2-t1)
        
    
    # the following two functions are used to simulate the real
    # scenario of random-walk forwarding
    def packetRandomInitialize( self, cnt = 50 ):
        ''' Randomly chose cnt cars to initialize one packet to
            forward in the VANET simulator '''
        mes_queue = []
        cars_id = copy.copy(self.cars.keys())
        while cnt > 0:
            tid = random.choice(cars_id)
            cars_id.remove( tid )
            c = self.cars[tid]
            m = c.msg_initialize()
            mes_queue.append(m)
            cnt -= 1
            # print c.cid, "has initialized one packet"
        
        return mes_queue
        
        
    def packetRandomWalkForwarding( self, ap_flag = True, 
                thBegin = 0, thEnd = 20, tsInterval = 600 ):
        '''This function simulate the scenario of random
            walk forwarding'''
        cars = self.cars.values()
        if ap_flag == True:
            ap = self.APs[0]
            ap_range = ap.getCurGeoHashRegion()
        
        t = thBegin * 3600
        tEnd = thEnd * 3600
        while t <= tEnd:
            self.setCurTime( t )
            print_time_f = False
            for c in cars:
                cnt = 0
                
                if c.msg_queue != []:
                    # send the packet to the AP if possible
                    if ap_flag == True and c.getCurGeoHash() in ap_range:
                        cnt = c.msg_send( ap )
                        if print_time_f == False:
                            print "At timestamp", t
                            print_time_f = True
                        print c.cid, "sends", cnt, "packets to AP"
                    else:
                    # forward packets between cars
                        neighbors = self.getNeighborsGeo( c.cid )
                        if neighbors != []:
                            cnt, d = c.msg_forward( neighbors )
                            if cnt != 0 and d != []:
                                if print_time_f == False:
                                    print "At timestamp", t
                                    print_time_f = True
                                print c.cid, 'sends', cnt, 'packets to', d.cid
            t += tsInterval
            
    def generateFilteredTrace( self, thBegin = 6, thEnd = 18, 
                    path ='E:\SH_TAXI_SELECTED\Taxi_070218_' ):
        self.trace.traceFilter( path, thBegin, thEnd )