% first execute the python simulator to get the l1_ls parameters
display( 'Execute the mainSim' );
system('python mainSim.py 1000 1000 &');

% wait the python process to end
flag = true;
while flag
    display('mainSim is still running. Check every 60 seconds');
    flag = isprocess('python.exe'); % isprocess is the function attached.
    pause(60); % check every 10 second
end

display( 'MainSim execute finished.' );


% initialize the local variable here
precision_cnt = zeros(10,1);
precision_rate = zeros(10,1);
% get Haar Trasformation Basis   htb
load Haar_Basis_1000.mat;


for index = 1:1:10
    % import the calculation result of the python simulator
    v = importdata( strcat('../tmp/RT_v_rec', num2str(index*10), '.txt') );
    v = v';
    A = importdata( strcat('../tmp/RT_A_rec', num2str(index*10), '.txt') );

    A = A*inv(htb)';    
    y = importdata( strcat('../tmp/RT_y_rec', num2str(index*10), '.txt') );
    y = y';

    display( length(y) );

    p_r = zeros(10,1);
    exp_id = 1;
    theta = SolveBP( A, y, 1000);
    x = inv(htb)' * theta;
    epsilon = x./v;


    len = length(x);
    precision_cnt(index) = 0;
    for i = 1:len
        if (abs(x(i) - v(i)) <= 1) || (epsilon(i) > 0.5 && epsilon(i) <= 2)
            precision_cnt(index) = precision_cnt(index) + 1;
        end
    end
    precision_rate(index) = precision_cnt(index) / len;
    p_r(exp_id) = precision_rate(index);


end