# built-in package
import fileinput
import geohash
import random
import copy
import dircache
# performance evaluation package
import time
# my package
import sys

from TraceTracker.record import record
from TraceTracker.car import car
from TraceTracker.AP import AP
from TraceTracker.simulator import simulator
from TraceTracker.analyzer import *
    
if __name__ == "__main__":
    t1 = time.clock()
    if len( sys.argv ) == 3:
        total_car = int( sys.argv[1] )
        total_packet = int( sys.argv[2] )
    else:
        total_car = 1000
        total_packet = 1000
        
    data_src = r'E:\SH_TAXI_SELECTED\Taxi_070218'
    filtered_data_src = r'E:\SH_TAXI_SELECTED\Taxi_070218_7h_19h'
    
    MyNaiveSim = simulator(filtered_data_src, 0, total_car)
    # MyNaiveSim.delegation_forwarding_parameter()
    
    # generate the timely filted trace once and for all
    # this function doesn't need to initiate vehicles
    # MyNaiveSim = simulator( data_src, 0, 2 )
    # MyNaiveSim.generateFilteredTrace( 8, 18 )

    # get the broken file list and remove the pitfalls
    # in the data
    # brokend = MyNaiveSim.getBrokenFile( )
    # MyNaiveSim.deleteBrokenFile()

    # neighborhood analysis 
    # MyNaiveSim.carsNeighborAnalysis()

    # geometric center for AP
    # AP_loc = MyNaiveSim.searchGeometricCenter(300)
    
    # neighbor of the AP
    # MyNaiveSim.findAPNeighbor()    
    
    # randomly generate packets
    # mqs = MyNaiveSim.packetRandomInitialize( total_packet )
    # forwarding the packets with random walk    
    # MyNaiveSim.packetRandomWalkForwarding( False, 6, 10, 60 )

    
    # ==================================================================
    # The following two assistant function are from the analyzer package
    # ==================================================================
    # generate l1_ls parameters
    # cars = MyNaiveSim.cars.values()
    # msgs = l1_ls_parameterGen( cars, mqs )

    # generate entropy analysis parameters
    # v = entropy_parameterGen( cars, 0, 24 )
    
    t2 = time.clock()      
    print "Totally using time ", t2 - t1
        
