clear;
SparsePath;
clc

%% original velocity, Haar transformation recover
% 
% In this section, I give 3 different usage for Haar Transformation
% A. directly recover the V
% B. recover the theta of original V ( theta = inv(psi) * V )
% C. recover the theta of sorted V
% initialize the local variable here

precision_rate = zeros(10,1);
error = zeros(10,1);
% initialize the transfromation basis
load Haar_Basis_1000.mat;
psi = inv(htb)';

precision_rate_1 = zeros(10,1);
precision_rate_2 = zeros(10,1);
precision_rate_3 = zeros(10,1);
load Haar_Basis_1000.mat;   % get htb
psi = inv(htb');    % psi is the basis of Haar Transformation

for index = 1:1:10
    % import the calculation result of the python simulator
    v = importdata( strcat('v_rec', num2str(index*10), '.txt') );
    v = v';
    [vs,vi] = sort(v);
    t = eye(length(v));
    t = t(vi,:);    % t is the basis of sorting
  
    A = importdata( strcat('A_rec', num2str(index*10), '.txt') );
    phi = A*psi;    % phi is the basis for Haar coefficient
    phi_sorted_haar = (A/t) * psi;
    
    y = importdata( strcat('y_rec', num2str(index*10), '.txt') );
    y = y';

    display( length(y) );
    % [x, status] = l1_ls_nonneg( A, y, 0.001, 0.001, true);
    x = SolveBP(A,y,length(v));
    precision_rate_1(index) = norm(x-v) / norm(v);
    
    theta = SolveBP(phi,y,length(v));
    x = psi*theta;
    precision_rate_2(index) = norm(x-v) / norm(v);
    
    theta = SolveBP(phi_sorted_haar,y,length(v));
    x = psi*theta;
    precision_rate_3(index) = norm(x-vs) / norm(vs);
end

clear psi theta index;

%% other recover method (upper bound)
% set missing as 0
precision_rate_4 = zeros(10,1);
precision_rate_5 = zeros(10,1);

for i = 1:1:10
    v = importdata( strcat('v_rec', num2str(i*10), '.txt') );
    v = v';
    r = floor(rand(1000-i*100,1)*999)+1;   % generate the unsampled
    avr = (sum(v)-sum(v(r)))/(i*100);   % the average of sampled
    x = v;
    x(r) = avr;     % set the unsampled value as avr
    precision_rate_4(i) = norm(x-v)/norm(v); 
    
    x = v;
    x(r) = 0;
    precision_rate_5(i) = norm(x-v)/norm(v);
end

%% call the iterated Haar recover algorithm
iterated_haar_recover;

%% Fourier transform recover
%
% In this section I recover in the Discret Fourier transformation space
% y = A * psi * theta, where ``psi'' is the DFT basis
precision_rate_6 = zeros(10,1);
load Fourier_Basis_1000; % get ftb
psi = inv(ftb);
psi = [real(psi), -imag(psi)];

for i = 1:1:10
    v = importdata( strcat('v_rec',num2str(i*10),'.txt') );
    v = v';
    A = importdata( strcat('A_rec', num2str(i*10), '.txt') );
    phi = A * psi;
    y = importdata( strcat('y_rec', num2str(i*10), '.txt') );
    y = y';
    
    theta = SolveBP(phi, y, 2*length(v),70,2);
    x = psi * theta;
    precision_rate_6(i) = norm(x-v)/norm(v);
end

clear i;

%% plot
figure;
plot( precision_rate_1, 'ro-');
hold on;
plot( precision_rate_2, 'b*-');
plot( precision_rate_3, 'gd-');
plot( precision_rate_4, 'cx-');
plot( precision_rate_5, 'k+-');
plot( precision_rate, 'ms-');
plot( precision_rate_6, 'g+-');
axis([0,10,0,1.2]);
set( gca, 'XTick', 0:1:10 );
set( gca, 'XTickLabel', {'0', '10', '20', '30', '40', '50', '60',  ...
       '70', '80', '90', '100' } );
xlabel('Collected Packets Percentage (%)');
ylabel('L^2 norm Error Rate');
title('L^2 Norm Recover Precision against Collected Packets');
legend('CS on original', 'CS on Haar Wavelet', ...
    'CS on Sorted Haar Wavelet','Fill average','Fill zero', ...
    'CS on iterated sorting Haar Wavelet', ...
    'CS on Fourier transform');
