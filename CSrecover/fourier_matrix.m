function FM = fourier_matrix( n )
% Generate an N by N orthonormal Fourier Basis fm, where   
%   fx = fm * x
%   -- fx is the fourier transformation result
%   -- x is the orginal vector
%
% reference:
% http://en.wikipedia.org/wiki/DFT_matrix
%
% Hongjian Wang
% sishenhadesi@163.com
FM = zeros(n);

for row = 1:n
    for col = 1:n
        % ============================================================
        % FM(row,col) = exp(-2*pi*1i*(row-1)*(col-1)/n) / sqrt(n);
        % ============================================================
        % we did not divide sqrt(n) from the result, for the fft()
        % in Matlab does not do so.
        % In fact, the normalization factor is just a convention, which
        % differs in different treatments.
        FM(row,col) = exp(-2*pi*1i*(row-1)*(col-1)/n);
    end
end

end

