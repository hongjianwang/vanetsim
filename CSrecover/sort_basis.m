function [ stb, ind ] = sort_basis( x )
if length(x) == sum(x==0)
    stb = eye(length(x));
else
    [~,ind] = sort(x);
    stb = eye(length(ind));
    stb = stb(ind,:);
    stb = inv(stb);
end

