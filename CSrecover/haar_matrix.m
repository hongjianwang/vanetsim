function HM = haar_matrix( n )
% Generate the n x n Haar Transform Basis
%   hx = x * hm
%       hx -- the row vector of Haar Space coefficient
%       x  -- the row vector of original vector
%
% Hongjian Wang
% sishenhadesi@163.com
if ( mod(n, 2) == 1 )
    HM = 'N is not even';
    return;
end

a = [ 0.5, 0.5; 0.5, -0.5 ];
num_a = floor( n/2 );
HM = eye(n);

while ( num_a >= 1 )
    % first we get the block diagnol transform matrix  
    transform_matrix = [];
    for i = 1:num_a
        transform_matrix = blkdiag( transform_matrix, a );
    end
    transform_matrix = blkdiag( transform_matrix, eye( n - num_a * 2 ) );

    % second we swap the corresponding columns
    swap_matrix = eye( n );
    swap_matrix = swap_matrix(:,[1:2:num_a*2, 2:2:num_a*2, num_a*2+1:n]);

    HM = HM * transform_matrix * swap_matrix;
    num_a = floor( num_a / 2 );
end

end