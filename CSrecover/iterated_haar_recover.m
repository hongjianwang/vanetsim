% Iterated Haar Transformation recover for Compressive Sensing.
% Because we can't get an ordered target vector, so we use the 
% iterated method to get a partial ordered target vector in 
% each iteration. Then make use of this order information in
% the next iteration.
% Hongjian Wang
% sishenhadesi@163.com
SparsePath;
clc;

precision_rate = zeros(10,1);
load Haar_Basis_1000.mat;   % get htb
psi = inv(htb');    % psi is the basis of Haar Transformation

for index = 1:10
    % import the calculation result of the python simulator
    v = importdata( strcat('v_rec', num2str(index*10), '.txt') );
    v = v';

    A = importdata( strcat('A_rec', num2str(index*10), '.txt') );

    y = importdata( strcat('y_rec', num2str(index*10), '.txt') );
    y = y';
    x = zeros(size(v));

    for i = 1:1:30
        display( length(y) );
        % [x, status] = l1_ls_nonneg( A, y, 0.001, 0.001, true);
        stb = sort_basis(x);
        Phi = A * stb * psi;
        theta_s = SolveBP(Phi,y,length(v), 50, 2);
        if norm(stb*psi*theta_s - x) < 50
            break;
        else
            x = stb * psi * theta_s;
        end
        precision_rate(index) = norm(x-v) / norm(v);
    %     figure;
    %     hold on;
    %     plot(inv(stb)*v,'g*');
    %     plot(theta_s,'b+');
    end
% figure;
% plot(precision_rate);
end

clear x stb psi theta_s v A y htb;