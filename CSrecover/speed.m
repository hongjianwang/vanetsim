v = importdata('velocities.txt');
v = v(200,:);
v = v';

load Haar_Basis_1000.mat;
htb = htb';

hv = htb * v;
figure;
hold on;
plot(v,'b+');
plot(hv, 'rx');
title('velocity, Haar wavelet on velocity');
legend({'Original velocity','Haar wavelet on velocity'});

vs = sort(v);
hvs = htb * vs;
figure;
hold on;
plot(vs,'b-');
plot(hvs, 'r-');
title('Sorted velocity, Haar wavelet on sorted velocity');
legend({'Sorted velocity','Haar wavelet on sorted velocity'});

fv = fft(v);
fv = real(fv);
figure;
hold on;
plot(vs,'b-');
plot(fv,'rx');
title('Velocity, Fourier on velocity');
legend({'Velocity','Fourier on velocity'});

fv = fft(vs);
fv = real(fv);
figure;
hold on;
plot(vs,'b-');
plot(fv,'rx');
title('Sorted velocity, Fourier on velocity');
legend({'Sorted velocity','Fourier on velocity'});