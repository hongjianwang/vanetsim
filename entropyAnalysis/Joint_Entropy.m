function H = Joint_Entropy(X, cenc, method)
% Returns joint entropy (in bits) of each column of 'X'
% by Hongjian Wang
%
% H = Joint_Entropy(X, n)
%
% H = row vector of calculated entropies (in bits)
% X = data to be analyzed
% cenc = conditional entropy for n > 1 columns
%
%   The conditional entropy is implemented in this way:
%       I'll get every n columns in X;
%       do the frequency and probability analysis
% ====================================================
%          method: 'combination' 'random' 'knn'
% ====================================================
% Last modified: Mar/21/2013

% Establish size of data
[n, m] = size(X);

joint_entropy = zeros(1,n);
interval = 1;
if nargin == 3 && strcmp(method,'combination') == 1
    cbn_index = combnk(1:size(X,1), cenc);
    for Row = 1:length(cbn_index)
        % build the conditional tuple vector
        conditionTupleVector = X(cbn_index(Row,:),:);
        conditionTupleVector = conditionTupleVector';
        % we use the interval as state
        if interval ~= 1
            conditionTupleVector = floor(conditionTupleVector / interval);
        end
        cdv = zeros( size(conditionTupleVector(:,1)) );
        % ==================================================
        % Based on the coding idea:
        %               any v, v < 300
        % use code function to 
        % transform a multiple tuple <a,b,c ...> tuple into 
        % a single number cdv = a*300^i + b*300^(i-1)
        % ==================================================
        for i = cenc:-1:1
          cdv = cdv + conditionTupleVector(:,i)*300^(i-1);
        end
        % Assemble observed alphabet
        Alphabet = unique(cdv);

        % Housekeeping
        Frequency = zeros(size(Alphabet));

        % Calculate sample frequencies
        for symbol = 1:length(Alphabet)
            Frequency(symbol) = sum( cdv == Alphabet(symbol) );
        end

        % Calculate sample class probabilities
        P = Frequency / sum(Frequency);

        % Calculate joint entropy in bits
        % Note: floating point underflow is never an issue since we are
        %   dealing only with the observed alphabet
        joint_entropy(Row) = -sum(P .* log2(P));
        % disp(['Finished the ' num2str(Column) ' column with joint entropy ' ...
        %    num2str(joint_entropy(Column))] );

    end
elseif nargin == 2 || (nargin == 3 && strcmp(method,'random') == 1)
    for Row = 1:n
        % build the conditional tuple vector
        conditionTupleVector = X(Row,:);
        for i = 1:cenc-1
            conditionTupleVector = [ X(mod(Row-1-i,n)+1,:); ...
                        conditionTupleVector];
        end
        conditionTupleVector = conditionTupleVector';
        % we use the interval as state
        if interval ~= 1
            conditionTupleVector = floor(conditionTupleVector / interval);
        end
        cdv = zeros( size(conditionTupleVector(:,1)) );
        % ====================================================
        % Based on the coding idea:
        %               any v, v < 300
        % use code function to 
        % transform a multiple tuple <a,b,c ...> tuple into 
        % a single number cdv = a+300*i + b+300*(i-1)
        % ====================================================
        for i = cenc:-1:1
          cdv = cdv + conditionTupleVector(:,i)*300^(i-1);
        end
        % Assemble observed alphabet
        Alphabet = unique(cdv);

        % Housekeeping
        Frequency = zeros(size(Alphabet));

        % Calculate sample frequencies
        for symbol = 1:length(Alphabet)
            Frequency(symbol) = sum( cdv == Alphabet(symbol) );
        end

        % Calculate sample class probabilities
        P = Frequency / sum(Frequency);

        % Calculate joint entropy in bits
        % Note: floating point underflow is never an issue since we are
        %   dealing only with the observed alphabet
        joint_entropy(Row) = -sum(P .* log2(P));
        % disp(['Finished the ' num2str(Column) ' column with joint entropy ' ...
        %    num2str(joint_entropy(Column))] );

    end
elseif nargin == 3 && strcmp(method, 'knn')
    nns = knnsearch(X,X,'k',cenc);
    nns = nns(:,2:cenc);
    
    for Row = 1:n
        % get one sample column
        sampleRow = X(Row, :);
        % build the conditional tuple vector
        conditionTupleVector = sampleRow;
        for i = 1:cenc-1
            conditionTupleVector = [X(nns(Row,i),:); ...
                        conditionTupleVector ];
        end
        conditionTupleVector = conditionTupleVector';
        % we use the interval as state
        if interval ~= 1
            conditionTupleVector = floor(conditionTupleVector / interval);
        end
        cdv = zeros( size(conditionTupleVector(:,1)) );
        % ==================================================
        % Based on the coding idea:
        %               any v, v < 300
        % use code function to 
        % transform a multiple tuple <a,b,c ...> tuple into 
        % a single number cdv = a*300^i + b*300^(i-1)
        % ==================================================
        for i = cenc:-1:1
          cdv = cdv + conditionTupleVector(:,i)*300^(i-1);
        end
        % Assemble observed alphabet
        Alphabet = unique(cdv);

        % Housekeeping
        Frequency = zeros(size(Alphabet));

        % Calculate sample frequencies
        for symbol = 1:length(Alphabet)
            Frequency(symbol) = sum( cdv == Alphabet(symbol) );
        end

        % Calculate sample class probabilities
        P = Frequency / sum(Frequency);

        % Calculate joint entropy in bits
        % Note: floating point underflow is never an issue since we are
        %   dealing only with the observed alphabet
        joint_entropy(Row) = -sum(P .* log2(P));
        % disp(['Finished the ' num2str(Column) ' column with joint entropy ' ...
        %    num2str(joint_entropy(Column))] );

    end
else
    error('Parameter error!');
end

% caculate the conditional entropy with
% conditionl entropy:
%          H(x1|x2) = H(x1, x2) - H(x1)
H = joint_entropy;



% God bless Claude Shannon.

% EOF


