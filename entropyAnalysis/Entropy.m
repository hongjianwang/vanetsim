function H = Entropy(X, cenc)
% Entropy: Returns entropy (in bits) of each column of 'X'
% by Hongjian Wang
%
% H = Entropy(X, n)
%
% H = row vector of calculated entropies (in bits)
% X = data to be analyzed
% cenc = conditional entropy for n > 1 columns
%
%   The conditional entropy is implemented in this way:
%       I'll get every contigious n columns in X;
%       do the frequency and probability analysis
% Last modified: Mar/21/2013



% Establish size of data
[~, m] = size(X);

% Housekeeping
H = zeros(1,m + 1 - cenc);

for Column = 1:(m+1-cenc),
    % get one sample column
    sampleColumn = X(:, Column:(Column+cenc-1));
    % Assemble observed alphabet
    Alphabet = unique(sampleColumn, 'rows');
	
    % Housekeeping
    Frequency = zeros(size(Alphabet(:,1)));
	
    % Calculate sample frequencies
    for index = 1:length(Alphabet(:,1))
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% sum ( [a1, a2, ... an ] == [b1, b2, ... bn] ) == n ) %%%
        %%%           refers to A == B                           %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        for i = 1:length( sampleColumn(:,1) )
            testFlag = sum( Alphabet(index, :) == sampleColumn(i, :));
            if testFlag == cenc
                Frequency(index) = Frequency(index) + 1;
            end
        end
    end
	
    % Calculate sample class probabilities
    P = Frequency / sum(Frequency);
	
    % Calculate entropy in bits
    % Note: floating point underflow is never an issue since we are
    %   dealing only with the observed alphabet
    H(Column) = -sum(P .* log2(P));
end


% God bless Claude Shannon.

% EOF


