function H = Conditional_Entropy(X, cenc, pre_entropy)
% Returns conditional entropy (in bits) of each column of 'X'
% by Hongjian Wang
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Attention: Please don't run this on more than 2 conditions  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% H = Entropy(X, n)
%
% H = row vector of calculated entropies (in bits)
% X = data to be analyzed
% cenc = conditional entropy for n > 1 columns
%
%   The conditional entropy is implemented in this way:
%       I'll get every contigious n columns in X;
%       do the frequency and probability analysis
% Last modified: Mar/21/2013


% Establish size of data
[~, m] = size(X);

% Housekeeping
joint_entropy = zeros(1,m);

for Column = 1:m
    % get one sample column
    sampleColumn = X(:, Column);
    % build the conditional tuple vector
    conditionTupleVector = combnk( sampleColumn, cenc );
    % within interval [x, x+10] will be treated as in the same state
    conditionTupleVector = floor( conditionTupleVector / 10 );
    cdv = zeros( size(conditionTupleVector(:,1)) );
    %
    % Based on the assumption:
    %               any v, v < 300
    % use code function to 
    % transform a multiple tuple <a,b,c ...> tuple into 
    % a single number cdv = a+300*i + b+300*(i-1)
    %
    for i = cenc:-1:1
      cdv = cdv + conditionTupleVector(:,i)*1000^(i-1);
    end
    % Assemble observed alphabet
    Alphabet = unique(cdv);
	
    % Housekeeping
    Frequency = zeros(size(Alphabet));
	
    % Calculate sample frequencies
    for symbol = 1:length(Alphabet)
        Frequency(symbol) = sum( cdv == Alphabet(symbol) );
    end

    % Calculate sample class probabilities
    P = Frequency / sum(Frequency);
	
    % Calculate joint entropy in bits
    % Note: floating point underflow is never an issue since we are
    %   dealing only with the observed alphabet
    joint_entropy(Column) = -sum(P .* log2(P));
    % disp(['Finished the ' num2str(Column) ' column with joint entropy ' ...
    %    num2str(joint_entropy(Column))] );
    
end

% caculate the conditional entropy with
% conditionl entropy:
%          H(x1|x2) = H(x1, x2) - H(x1)
H = joint_entropy - pre_entropy;
% God bless Claude Shannon.

% EOF


