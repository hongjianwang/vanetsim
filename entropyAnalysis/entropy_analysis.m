% Spatio Entropy Analysis
% ===================================================================
% We mine the Conditional Entropy of velocity Spatio Distribution.
% this file will read in the velocity matrix of each vechile at time t
% v = [ v1, v2, v3 ... ]
% where v_i is the set of all speed at time t_i
% This file do the following works:
%   1. calculate marginal entropy H(v_i)
%   2. calculate 2-joint entropy H(v_i-1, v_i)
%   3. calculate conditional entropy 
%               H(v_i | v_i-1) = H(v_i-1, v_i) - H(v_i)
%   4. plot them all

% v = importdata('../tmp/RT_velocities.txt');
% v = v';
v = importdata('../tmp/RT_location_info.txt');
v = v(:,2:101);

display('Calculate marginal entropy ...');
h_m = Marginal_Entropy(v');

display('Calculate conditional entropy k = 2 ...');
% h_c2 = Conditional_Entropy( v, 2, h_m1 );
% h_j2_rand = Joint_Entropy(v,2,'random');
% h_c2_rand = h_j2_rand - h_m;

h_j2_knn = Joint_Entropy(v,2,'knn');
h_c2_knn = h_j2_knn - h_m;

%          H(x1|x2) = H(x1, x2) - H(x1)
display('Calculate conditional entropy k = 3 ...');
% h_j3_rand = Joint_Entropy(v,3,'random');
% h_c3_rand = h_j3_rand - h_j2_rand;

h_j3_knn = Joint_Entropy(v,3,'knn');
h_c3_knn = h_j3_knn - h_j2_knn;

h_j4_knn = Joint_Entropy(v,4,'knn');
h_c4_knn = h_j4_knn - h_j3_knn;

h_j5_knn = Joint_Entropy(v,5,'knn');
h_c5_knn = h_j5_knn - h_j4_knn;

[f1, x1] = ecdf(h_m);
[f2, x2] = ecdf(h_c2_knn);
[f3, x3] = ecdf(h_c3_knn);
[f4, x4] = ecdf(h_c4_knn);
[f5, x5] = ecdf(h_c5_knn);

figure;
hold on;
xLim([-0.2, 7]);
plot( x1, f1, 'b-', 'LineWidth', 2);
% plot( x2, f2, 'g-');
plot( x3, f3, 'r--', 'LineWidth', 2);
plot( x4, f4, 'k-', 'LineWidth', 2);
plot( x5, f5, 'm-.', 'LineWidth', 2);
% set( gca, 'XTick', 0:1:10 );
% set( gca, 'XTickLabel', {'0', '10', '20', '30', '40', '50', '60',  ...
%       '70', '80', '90', '100' } );
xlabel('Entropy (bits)', 'FontSize', 16);
ylabel('CDF', 'FontSize', 16);
% title('CDF of Marginal and Conditional Entropy with Interval 1');
legend({'Marginal', ...  % 'Conditional c = 1', ...
        'Conditional c = 2',  ...
        'Conditional c = 3', ...
        'Conditional c = 4'}, 'Location', 'Best', 'FontSize', 12, ...
        'FontWeight', 'Bold');
legend('boxoff');
set(gca, 'FontSize', 15, 'LineWidth', 2);
box on;
grid on;