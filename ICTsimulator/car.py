# class car describe one car
# each car has its own trajectory, which consists of record
# The car class is used for the Inter-contact time based simulation model.

import math
import random

class car:
    ''' class car assemble all the behavior of a vehicle in the VANET '''
    
    def __init__(self, cid = 0, reading = 0):
        ''' Initiate car instance, with data attribute:
            cid
            msg_queue
            reading
        '''
        self.cid = cid
        self.msg_queue = list()
        self.reading = reading  # we evaluate an ideal data
        

    def __str__(self):
        ''' wrapper instance into a string to print in the console ''' 
        return ('#car: ' + str(self.cid) +', reading: ' +
            str(self.reading) )

    def msg_initialize( self ):
        ''' initialize one piece of message ''' 
        # assign norm distribution rand
        self.rc = random.normalvariate(0, 0.8) 
        val = self.reading * self.rc
        self.msg_queue.append( [val, (self.cid, self.rc)] )
        return self.msg_queue
    
    def msg_receive( self, msg ):
        ''' receive one piece of message '''
        # assign norm distribution rand
        self.rc = random.normalvariate(0, 0.8) 
        msg[0] += self.reading * self.rc
        msg.append( (self.cid, self.rc) )
        self.msg_queue.append( msg )

    def msg_send( self, msgs, dst ):
        ''' send all messages in message queue to dst,
            single copy, without backup '''
        cnt = 0
        for msg in msgs:
            if msg in self.msg_queue and \
                    dst.cid not in [ c[0] for c in msg[1:] ]:
                dst.msg_receive( msg )
                self.msg_queue.remove( msg )
                cnt += 1
        return cnt
            
            