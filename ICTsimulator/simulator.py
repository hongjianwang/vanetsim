####################################################
#   Inter Contact Time driven VANET model simulator
#   
#   Hongjian Wang
#   sishenhadesi@163.com
####################################################
from array import array
from random import random, randint, choice
from car import car
from math import pi, sin, log
from time import clock

class simulator:
    ''' VANET simulator based on Inter-Contact Time model,
        with data attribute:
            lambdas = list()
            event_pool = list()
            cars = list()
            msg = list()
    '''
    
    def __init__( self, n ):
        ''' Initiate all the components in simulator '''
        
        def interpolation( i ):
            ''' interpolate function for sensor readings '''
            return          sin(2*pi/1000 * i) \
                    + 0.6 * sin(6*pi/1000 * i - pi / 6 )
            # return 2.71 ** ( - i/ 200.0 )
        
        t1 = clock()
        print 'Simulator construct begins ...'
        # initiate all cars
        self.cars = list()
        for i in range(0, n):
            self.cars.append( car(i, interpolation(i) ) )
            
        # initiate the ICT coefficient matrix
        self.lambdas = list()
        for i in range(0, n):
            self.lambdas.append( array('d') )
            for j in range(0, n):
                if i == j:
                # diagonal should be zero
                    self.lambdas[i].append(0)
                elif random() > 0.9:
                # vehicles has usable ICT with 1/10 probability
                    self.lambdas[i].append( random() / 10**3 )
                else:
                    self.lambdas[i].append( random() / 10**8 )
        
        # save the original readings
        fout = open('../tmp/ICT_readings.txt','w')
        for c in self.cars:
            fout.write( str(c.reading) + '\n' )
        fout.close()
        
        # initiate the encounter event pool
        self.event_pool = list()
        
        print 'Initialization finished within %g seconds' % \
                (clock()-t1)
        
    def init_msgs( self, k ):  
        ''' 
            initiate the k simulator msgs, we will clear all the message`
            queues first, then initiate k new messages
        '''
        t1 = clock()
        print 'Message initiate begins ...'
        n = len(self.cars)
        # clear all the messge queues first
        for c in self.cars:
            c.msg_queue = []
            
        self.msg = []
        tmpcars = list(self.cars)
        if n - k > k:
            for i in range(0, k):
                c = choice(tmpcars)
                c.msg_initialize()
                self.msg.append( c.msg_queue[0] )
                tmpcars.remove(c)
        else:
            for i in range(0, n-k):
                tmpcars.remove( choice(tmpcars) )
            for c in tmpcars:
                c.msg_initialize()
                self.msg.append( c.msg_queue[0] )
                
        print 'Message initiate within %g seconds' % \
                (clock()-t1)
        
    
    def event_collector( self, t_interval ):
        ''' Collect all the encounter event into the event pool
        Initailly the collected event enqueues without the time ordering,
        then we sort the event pool to make them in order. '''
        
        def calcICT( p, lamb ):
            ''' helper function for event_collector().
            calculate the Inter-Contact Time for given probability and
            lambda'''
            return - log( 1-p )/lamb
        
        t1 = clock()
        print 'Event collect begins ...'
        n = len(self.cars)
        for i in range(0, n):
            for j in range(0, n):
                if j > i:
                    timeLine = 0
                    lambd = self.lambdas[i][j]
                    
                    p = random()
                    ict = calcICT( p, lambd )
                    timeLine += ict
                    while timeLine <= t_interval:
                        self.event_pool.append( (i,j, timeLine) )
                        p = random()
                        ict = calcICT( p, lambd )
                        timeLine += ict
                        
        # sorting
        self.event_pool = sorted( self.event_pool, key = \
                        lambda student: student[2] )
                        
        print 'Event collect finished within %g seconds' % \
                    (clock()-t1)
                        
    def event_handler( self ):
        '''
        Handler for the encounter event in the event_pool.
        '''
        t1 = clock()
        print 'Event handle begins ...'
        fout = open('../tmp/ICT_log.txt', 'w')
        
        for t in self.event_pool:
            iqueue = list( self.cars[t[0]].msg_queue )
            jqueue = list( self.cars[t[1]].msg_queue )
            if iqueue != []:
                n = self.cars[t[0]].msg_send( iqueue, self.cars[t[1]] )
                msg_log = 'At %g car %d send car %d %d messages.\n' % \
                        ( t[2], t[0], t[1], n )
                fout.write( msg_log )
                print msg_log
            if jqueue != []:
                n = self.cars[t[1]].msg_send( jqueue, self.cars[t[0]] )
                msg_log = 'At %g car %d send car %d %d messages.\n' % \
                        ( t[2], t[1], t[0], n )
                fout.write( msg_log )
                print msg_log
        fout.close()
        print 'Event handle finished within %g seconds' % \
                ( clock()-t1 )
                
    def parameterGenerate( self, path = '../tmp/', common_prefix = 'ICT_' ):
        '''
        Generate the parameters usded in compressive sensing.
                y = A x
        We put y in one file, A in one file.
        '''
        cars = self.cars
        n = len(cars)
        k = len(self.msg)
        fout = open(''.join( [path, common_prefix, 'y', str(k), '.txt']), 'w' )
        for m in self.msg:
            fout.write(str(m[0]) + '\n')
        fout.close()

        fout = open(''.join( [path, common_prefix, 'A', str(k), '.txt']) , 'w' )
        Ar = [ str(a*0) for a in range(1000) ]
        for m in self.msg:
            for c in m[1:]: # c is a tupel (cid, rc)
                Ar[c[0]] = str( c[1] )
            fout.write( '\t'.join(Ar) )
            fout.write('\n')
        fout.close()
        
        fout = open(''.join([path, common_prefix, 'msg', str(k), '.txt']), 'w' )
        for m in self.msg:
            for c in m[1:]:
                fout.write( str(c[0]) + '\t' )
            fout.write('\n')
        fout.close()
        