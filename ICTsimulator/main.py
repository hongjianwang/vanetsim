from simulator import simulator
from random import choice
import sys

def randomChoseN( lst, n ):
    ''' randomly chose n elements from lst '''
    t = list(lst)
    res = list()
    if n < len(lst) / 2:
        for i in range(n):
            a = choice(t)
            res.append( a )
            t.remove(a)
    else:
        k = len(lst) - n
        for i in range(k):
            t.remove( choice(t) )
        res = t
    return res

if __name__ == '__main__':
    ''' sys.argv[1] is the number of measurements in use '''
    if len( sys.argv ) < 2:
        n = 1000
    elif len( sys.argv ) == 2:
        n = int( sys.argv[1] )
        
    print 'main.py begin with para', sys.argv[1]
    sim = simulator(1000)
    sim.event_collector( 3600 * 24 )

    sim.init_msgs( n )
    sim.event_handler()
    sim.parameterGenerate()