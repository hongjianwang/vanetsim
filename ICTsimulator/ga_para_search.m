function [x, fval] = ga_para_search()
%{
Find optimal number of hops and measurements with genetic algorithm.

This function apply genetic algorithms to do the optimal searching. Nothing
more. Just a fancy technique for a normal problem.

Hongjian Wang
comments added on March 20, 2013
%}


% environmental variable
A_full = importdata('../tmp/ICT_A1000.txt');
r = importdata('../tmp/ICT_readings.txt');
msg = dlmread('../tmp/ICT_msg1000.txt','\t');
msg = msg(:,1:900);
% load Fourier transform parameters
addpath('../CSrecover/');
ftb = fourier_matrix(1000); % get ftb
psi = inv(ftb);
psi = [real(psi), -imag(psi)];

error = ones(900,1000) * -1;

Bound = [10,10; 900,1000]; % bounded for [h,m]

% Create an options structure to be passed to GA
% Three options namely 'CreationFcn', 'MutationFcn', and
% 'PopInitRange' are required part of the problem.
options = gaoptimset('CreationFcn',@int_pop,'MutationFcn',@int_mutation, ...
    'PopInitRange',Bound,'Display','iter','StallGenL',100,'Generations',80, ...
    'PopulationSize',100,'PlotFcns',{@gaplotbestf,@gaplotbestindiv});

[x, fval] = ga(@fitnessFunc, 2, options); 

    % ===========================================================
    % fitness function for genetic function
    % ===========================================================
    function val = fitnessFunc( x )
        if cs_estimate(x(1), x(2), 0.03) == 1
            val = x(1) + x(2);
        else
            val = 2000;
        end
    end
    % ===========================================================
    % compressive sensing estimate function
    % ===========================================================
    function er = cs_estimate( h, m, threshold )
        % fill up the 3x3 local error cube
        for i = h-1:1:h+1
            for j = m-1:1:m+1
                if error(i,j) == -1
                    [y, A] = subproblem( A_full, msg, r, i, j);
                    phi = A * psi;
                    theta = SolveBP(phi, y, 2*length(r));
                    rbar = psi * theta;
                    error(i,j) = norm(rbar-r)/norm(r);
                end
            end
        end
        for i = h-1:1:h+1
            for j = m-1:1:m+1
                if error(i,j) > threshold
                    er = 0;
                    return;
                end
            end
        end
        er = 1;
        return;
    end
    % ===========================================================
    % subfunction to construct the subproblem from full
    % ===========================================================
    function [y_sub, A_sub] = subproblem( A_full, msg_full, x, h, m )
        % subproblem constructor
        % construct:    y_sub = A_sub * x
        % from the given information on A_full and msg_full
        % and the scale of subproblem (h,m)
        
        % construct h-hop sub-msg
        msg_sub = msg_full(:, h);
        A_sub = zeros(size(A_full));
        for i = 1:size(A_sub,1)
            A_sub(i, msg_sub(i,:)+1) = A_full(i,msg_sub(i,:)+1);
        end     
        % construct m measurements A_sub
        m_ri = randperm( size(A_full, 1) );
        m_ri = m_ri(1:m);
        m_ri = sort(m_ri); 
        A_sub = A_sub(m_ri,:);
        % construct y_sub
        y_sub = A_sub * x;
    end

    %---------------------------------------------------
    % Mutation function to generate childrens satisfying the range and integer
    % constraints on decision variables.
    function mutationChildren = int_mutation(parents,options,GenomeLength, ...
        ~,state,~,~)
        shrink = .01; 
        scale = 1;
        scale = scale - shrink * scale * state.Generation/options.Generations;
        range = options.PopInitRange;
        lower = range(1,:);
        upper = range(2,:);
        scale = scale * (upper - lower);
        mutationPop =  length(parents);
        % The use of ROUND function will make sure that childrens are integers.
        mutationChildren =  repmat(lower,mutationPop,1) +  ...
            round(repmat(scale,mutationPop,1) .* rand(mutationPop,GenomeLength));
        % End of mutation function
    end

    %---------------------------------------------------
    function Population = int_pop(GenomeLength,~,options)

        totalpopulation = sum(options.PopulationSize);
        range = options.PopInitRange;
        lower= range(1,:);
        span = range(2,:) - lower;
        % The use of ROUND function will make sure that individuals are integers.
        Population = repmat(lower,totalpopulation,1) +  ...
            round(repmat(span,totalpopulation,1) .* rand(totalpopulation,GenomeLength));
        % End of creation function
    end
end