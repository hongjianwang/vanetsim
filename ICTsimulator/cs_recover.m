%{
======================================================================
    Inter-contact Time model based for compressive sensing simulator
                                         ( matlab analysis section )

    Author: Hongjian Wang
    e-mail: sishenhadesi@163.com
======================================================================
    Pre-condition (Work has done)
    In the python Simulator section of this ICT model based simulator,
I have generate all the contact event and data, which is A and y in
        y = A * x.
    In my settings:
    x is 1000-dimensions vector, which holds all 1000 vehicles sensor 
readings;
    y is 1000-dimensions vector, which refers to we have done 1000
measurements on these sensor readings;
    A is the random ensamble with 1000 x 1000.

    Besides, I can also get all the 1000 measurements messages, so 
that I can random choosen a subset of them as the k measurement cases.
----------------------------------------------------------------------
    Process (Work finished here)
    In this matlab script, I sketch the relationship among n, h and 
error rate, i.e.
        error_rate = g(m, h)
where n is the number of measurements and h is the number of involved
vehicles for each measurements (or number of hops).
----------------------------------------------------------------------
    Post-condition (Final result)
    Finally we should get a matrix tracking the error_rate, which is
        error_rate = g(m, h).
    We saved the result in a MAT-file named para.mat.
And g(n,h) should be an continuous function, which can use a surface
fitting method to get the surface function.
    That surface fiting function is the final result we need.
%}
%{
==============================================
    execute the python simulator in matlab
==============================================
system( strcat(['python main.py ', num2str(n(j)),' &']) );

flag = true;
while flag
    flag = isprocess('python.exe'); % isprocess is the function attached.
    pause(2); % check every 10 second
end
%}
% call the python simulator and cause MATLAB blocked waiting
% [a, b] = dos('python main.py 1000', '-echo');

% clear the working environment
SparsePath;
clear;
clc;

% load the file
Afull = importdata( '../tmp/ICT_A1000.txt' );
r = importdata('../tmp/ICT_readings.txt');
msg = dlmread('../tmp/ICT_msg1000.txt', '\t');

h = [5:5:30, 40:10:200];
num_h = length(h);
n = [50, 100:100:700, 700:50:950];
num_m = length(n);

error = zeros(num_m, num_h, 1);

% load Fourier transform parameters
addpath('../CSrecover/');
ftb = fourier_matrix(1000); % get ftb
psi1 = inv(ftb);
psi1 = [real(psi1), -imag(psi1)];

for m_iter = 1:1:num_m

    for h_iter = 1:1:num_h
        % construct the nxm matrix A with h hops
        % first construct h hops
        h_ind = msg(:,1:h(h_iter));
        A = zeros(size(Afull));
        for i = 1:size(A,1)
            A(i,h_ind(i,:)+1) = Afull(i,h_ind(i,:)+1);
        end
        
        m_ri = randperm(size(msg,1));
        m_ri = m_ri(1:n(m_iter));
        m_ri = sort(m_ri);
        A = A(m_ri,:);
        
        y = A*r;
        
%{
        % directly Compressive Sensing recovery
        rbar1 = SolveBP(A, y, length(r));
        % error1(i) = norm(rbar1 - r)/norm(r);
       

        % recover using Haar transform space
        load Haar_Basis_1000; % get htb
        psi2 = inv(htb');
        phi2 = A / (htb');    % A * psi2;
        theta2 = SolveBP(phi2, y, length(r));
        rbar3 = (htb') \ theta2;   % psi2 * theta2;
        error3(i) = norm(rbar3-r)/norm(r);
%}
        % recover using fourier transform space
        phi1 = A * psi1;
        theta1 = SolveBP(phi1, y, 2*length(r));
        rbar2 = psi1 * theta1; 
        error(m_iter, h_iter, 1) = norm(rbar2-r)/norm(r);
        
        
    end
    save('../tmp/ICT_para.mat','error','m_iter','h_iter');
    %{
    subplot(5,3,j);
    hold on;
    plot(error1, 'b-.');
    plot(error2, 'r-.');
    plot(error3, 'k-.');
    axis([0,10,0,1.2]);
    set( gca, 'XTick', 0:2:10 );
    set( gca, 'XTickLabel', {'0', '10', '20', '30', '40', '50'} );
    title(strcat('1000 cars ',...
            num2str(numContact(j)), ' contacts'));
    % legend('Direct recover', 'Fourier transform recover', ...
    %       'Haar transform recover')
    %}
end
